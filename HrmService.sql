Use HRM_service
go


CREATE TABLE department(				-- Table: Phong ban
	id int primary key identity,
	dep_name varchar(100) not null,		-- ten phong ban
	dep_manager_id int not null,		-- id truong phong
	status tinyint default 1,			-- trang thai: 0: disable, 1 : enabled
	created_id int,						-- nguoi tao ban ghi
	created_time datetime,				-- thoi gian tao ban ghi
	updated_id int,						-- nguoi chinh sua 
	updated_time datetime				-- thoi gian chinh sua
)
go

CREATE TABLE "role"(					-- Table: Chuc vu
	id int primary key identity,
	role_name varchar(32) not null,		-- ten chuc vu
)
GO
-- abcdef
CREATE TABLE permission(				-- Table: Quyen han
	id int primary key identity, 
	name nvarchar(100) not null,		-- Ten quyen han
	permission_code nvarchar(32) null,	-- Ma quyen han
	"description" nvarchar(254) null	-- Mo ta chi tiet
)
GO
-- bghjghjghjhj--
CREATE TABLE role_permission(			-- Table: bang phu giua role & permission
	role_id int,
	permission_id int,
	primary key(role_id, permission_id),
	CONSTRAINT fk_role_permission_permission_id FOREIGN KEY (role_id) REFERENCES permission(id) ,
	CONSTRAINT fk_role_permission_role_id FOREIGN KEY (permission_id) REFERENCES "role"(id) ,
)
GO

CREATE TABLE "user"(						--Table: Nhan vien
	id int primary key identity,
	user_code int not null,					-- Ma nhan vien (dung de dang nhap)
	username varchar(100) not null,			-- Ten nhan vien
	email varchar(128) not null,			-- Email nhan vien (dung de dang nhap)
	"password" varchar(100) not null,		-- Mat khau dang nhap
	department_id int,						-- Ma phong ban cua nhan vien
	role_id int not null,					-- Chuc vu nhan vien
	"status" tinyint default 1,				-- Trang thai: 0: disabled, 1: enabled
	created_id int DEFAULT NULL,
	created_time datetime NULL,
	updated_id int DEFAULT NULL,
	updated_time datetime NULL,
	constraint fk_users_department_id
	foreign key (department_id) references department(id),	-- chieu toi bang department
	constraint fk_users_created_id
	foreign key (id) references [user](id),					-- chieu toi chinh bang user
	constraint fk_users_role_id
	foreign key (role_id) references [role](id),			-- chieu toi bang role
	constraint fk_users_updated_id
	foreign key (id) references [user](id)					-- chieu toi chinh bang user
)
GO

CREATE TABLE timesheet(						-- Table: chua thong tin: nghi phep, Overtime( lam them gio), di muon
	id int primary key identity,
	"user_id" int not null,					-- Ma nhan vien: chieu toi bang user
	user_reason varchar(255) not null,		-- Ly do dang ky timesheet
	"type" tinyint not null,				-- Loai timesheet:  0 -> di muon, 1 -> OT, 2 -> Nghi phep 
	time_start datetime not null,			-- Thoi gian bat dau timesheet
	time_end datetime not null,				-- Thoi gian ket thuc timesheet
	reject_reason varchar(255) default null,  -- Ly do timesheet bi tu choi ( danh cho cac timesheet khong duoc cap tren chap nhan)
	"status" tinyint not null,	/* Trang thai timesheet: 0 -> saved (chua gui cap tren, duoc luu tam chi minh nhin thay),
														 1 -> submited (da gui len cap tren va cho duyet)
														 2 -> approved (da duoc cap tren duyet va dong y)
														 3 -> reject (da duoc cap tren duyet va khong dong y)
								*/
	created_id int DEFAULT NULL,
	created_time datetime NULL,
	updated_id int DEFAULT NULL,
	updated_time datetime NULL
	constraint fk_timesheets_created_id
	foreign key (created_id) references [user](id),
	constraint fk_timesheets_updated_id
	foreign key (updated_id) references [user](id),
	constraint fk_timesheets_user_id
	foreign key ("user_id") references [user](id)
)
GO

DROP table timesheet

CREATE TABLE user_current(							-- Table: luu nguoi dung dang login
	id int primary key identity,
	"user_id" int not null,							-- id nguoi dung
	login_time datetime not null,					-- thoi gian dang nhap
	"status" tinyint default 1, 
	constraint fk_users_current_id
	foreign key ([user_id]) references [user](id)
)
GO

ALTER TABLE department
add constraint fk_departments_created_id
foreign key (created_id) references [user](id)
GO

ALTER TABLE department
add constraint fk_departments_department_manager_id
foreign key (dep_manager_id) references [user](id)
GO

ALTER TABLE department
add constraint fk_departments_updated_id
foreign key (updated_id) references [user](id)
GO


